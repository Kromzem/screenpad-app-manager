﻿namespace Screenpad_App_Manager.Extensions {
    public static class Extensions {
        public static int ToInt(this string str) {
            return int.Parse(str);
        }
    }
}