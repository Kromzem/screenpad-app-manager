﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Screenpad_App_Manager.Extensions;

namespace Screenpad_App_Manager {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow {
        private string _iniFile = null;

        public MainWindow() {
            InitializeComponent();

            Task.Run(SearchForIniFile);
        }

        private void SearchForIniFile() {
            var files = GetFiles().ToList();
            
            Application.Current.Dispatcher.Invoke(() => {
                if (files.Count == 0) {
                    Searching.Content = "Couldn't find ScreenPadLauncher.ini :(";

                    return;
                }

                if (files.Count > 1) {
                    Searching.Content = "Multiple ScreenPadLauncher.ini files found o.O";

                    return;
                }

                Searching.Content = $"Found: {files[0]}";
                _iniFile = files[0];
            });
        }

        private IEnumerable<string> GetFiles() {
            Directory.GetFiles(
                $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}\\AppData\\Local",
                "ScreenPadLauncher.ini");

            var pending = new Stack<string>();
            pending.Push($"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}\\AppData\\Local");

            while (pending.Count > 0) {
                var dir = pending.Pop();
                string[] files = null;
                try {
                    files = Directory.GetFiles(dir, "ScreenPadLauncher.ini");
                } catch {
                    //ignored
                }

                if (files != null) {
                    foreach (var file in files) {
                        yield return file;
                    }
                }

                try {
                    var dirs = Directory.GetDirectories(dir);
                    foreach (var newDir in dirs) {
                        pending.Push(newDir);
                    }
                } catch {
                    // ignored
                }
            }
        }

        private void App_Drop(object sender, DragEventArgs e) {
            if (_iniFile == null) {
                MessageBox.Show("There's no ini file available ...", "Too bad", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);

                return;
            }

            Debug.WriteLine("Dropped success!");
            var lines = File.ReadLines(_iniFile);

            var sections = new Dictionary<string, Dictionary<string, string>>();
            var activeSection = "";
            foreach (var line in lines.Where(x => !string.IsNullOrEmpty(x))) {
                if (line.StartsWith("[") && line.EndsWith("]")) {
                    activeSection = line;
                } else {
                    if (!sections.TryGetValue(activeSection, out var section)) {
                        section = new Dictionary<string, string>();
                        sections[activeSection] = section;
                    }

                    var index = line.IndexOf('=');
                    section[line.Substring(0, index)] = line.Substring(index + 1);
                }
            }

            if (!(sender is Button button)) return;

            var number = button.Name.Split('_')[1].ToInt();

            var filePath = (e.Data.GetData(DataFormats.FileDrop) as string[])?[0];
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            Debug.WriteLine(filePath);
            var program = sections[$"[Page0-{number}]"];
            program["ExeAbsolutePath"] = filePath;
            program["AppName"] = fileName;
            program["ColorA"] = "255";
            program["ColorR"] = "0";
            program["ColorG"] = "0";
            program["ColorB"] = "0";
            program["PackageFullName"] = "";
            program["PackageFamilyName"] = "";
            program["AumidPath"] = "";
            program["AppLogoPath"] = "";

            File.WriteAllLines(_iniFile, GetUpdatedSections());

            var processes = Process.GetProcessesByName("AsusScreenPad");
            foreach (var p in processes) {
                p.Kill();
            }

            Thread.Sleep(100);
            Process.Start($"{Directory.GetCurrentDirectory()}\\AsusScreenPad.exe");

            IEnumerable<string> GetUpdatedSections() {
                var sects = new List<string>();
                sects.AddRange(GetUpdatedSection("[Info]"));
                sects.AddRange(GetUpdatedSection("[Setting]"));

                for (var i = 0; sections.ContainsKey($"[App{i}]"); i++) {
                    sects.AddRange(GetUpdatedSection($"[App{i}]"));
                }

                sects.AddRange(GetUpdatedSection("[Update]"));

                for (var page = 0; sections.ContainsKey($"[Page{page}-0]"); page++) {
                    for (var app = 0; sections.ContainsKey($"[Page{page}-{app}]"); app++) {
                        sects.AddRange(GetUpdatedSection($"[Page{page}-{app}]"));
                    }
                }

                return sects;
            }

            IEnumerable<string> GetUpdatedSection(string sectionName) {
                yield return sectionName;
                foreach (var parameter in sections[sectionName]) {
                    yield return $"{parameter.Key}={parameter.Value}";
                }
            }
        }

        private void App_DragEnter(object sender, DragEventArgs e) {
            e.Effects = DragDropEffects.None;
            e.Handled = true;

            if (!e.Data.GetDataPresent(DataFormats.FileDrop)) {
                return;
            }

            var filePath = (e.Data.GetData(DataFormats.FileDrop) as string[])?[0];
            if (string.IsNullOrEmpty(filePath)) {
                return;
            }

            if (Path.GetExtension(filePath) != ".exe") {
                return;
            }

            e.Handled = false;
            e.Effects = DragDropEffects.Copy;
        }
    }
}