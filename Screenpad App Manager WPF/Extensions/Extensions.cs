﻿namespace Screenpad_App_Manager_WPF.Extensions {
    public static class Extensions {
        public static int ToInt(this string str) {
            return int.Parse(str);
        }
    }
}