﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Screenpad_App_Manager_WPF.Extensions;

namespace Screenpad_App_Manager_WPF {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private async void App_Drop(object sender, DragEventArgs e) {
            Debug.WriteLine("Dropped success!");
            var settingsPath =
                $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}\\AppData\\Local\\ASUS\\ScreenXpert\\ScreenPadLauncher.ini";

            var lines = File.ReadLines(settingsPath);

            var sections = new Dictionary<string, Dictionary<string, string>>();
            var activeSection = "";
            foreach (var line in lines.Where(x => !string.IsNullOrEmpty(x))) {
                if (line.StartsWith('[') && line.EndsWith(']')) {
                    activeSection = line;
                } else {
                    if (!sections.TryGetValue(activeSection, out var section)) {
                        section = new Dictionary<string, string>();
                        sections[activeSection] = section;
                    }

                    var splitted = line.Split("=", 2);
                    section[splitted[0]] = splitted[1];
                }
            }

            if (!(sender is Button button)) return;

            var number = button.Name.Split("_")[1].ToInt();

            var filePath = (e.Data.GetData(DataFormats.FileDrop) as string[])?[0];
            var fileName = Path.GetFileNameWithoutExtension(filePath);
            Debug.WriteLine(filePath);
            var program = sections[$"[Page0-{number}]"];
            program["ExeAbsolutePath"] = filePath;
            program["AppName"] = fileName;
            program["ColorA"] = "255";
            program["ColorR"] = "0";
            program["ColorG"] = "0";
            program["ColorB"] = "0";
            program["PackageFullName"] = "";
            program["PackageFamilyName"] = "";
            program["AumidPath"] = "";
            program["AppLogoPath"] = "";

            await File.WriteAllLinesAsync(settingsPath, GetUpdatedSections());
            
            var processes = Process.GetProcessesByName("AsusScreenPad");
            foreach (var p in processes) {
                p.Kill();
            }

            IEnumerable<string> GetUpdatedSections() {
                var sects = new List<string>();
                sects.AddRange(GetUpdatedSection("[Info]"));
                sects.AddRange(GetUpdatedSection("[Setting]"));

                for (var i = 0; sections.ContainsKey($"[App{i}]"); i++) {
                    sects.AddRange(GetUpdatedSection($"[App{i}]"));
                }

                sects.AddRange(GetUpdatedSection("[Update]"));

                for (var page = 0; sections.ContainsKey($"[Page{page}-0]"); page++) {
                    for (var app = 0; sections.ContainsKey($"[Page{page}-{app}]"); app++) {
                        sects.AddRange(GetUpdatedSection($"[Page{page}-{app}]"));
                    }
                }

                return sects;
            }

            IEnumerable<string> GetUpdatedSection(string sectionName) {
                yield return sectionName;
                foreach (var (key, value) in sections[sectionName]) {
                    yield return $"{key}={value}";
                }
            }
        }

        private void App_DragEnter(object sender, DragEventArgs e) {
            e.Effects = DragDropEffects.None;
            e.Handled = true;

            if (!e.Data.GetDataPresent(DataFormats.FileDrop)) {
                return;
            }

            var filePath = (e.Data.GetData(DataFormats.FileDrop) as string[])?[0];
            if (string.IsNullOrEmpty(filePath)) {
                return;
            }

            if (Path.GetExtension(filePath) != ".exe") {
                return;
            }

            e.Handled = false;
            e.Effects = DragDropEffects.Copy;
        }
    }
}