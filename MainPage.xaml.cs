﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Screenpad_App_Manager.Extensions;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Screenpad_App_Manager {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page {
        public MainPage() {
            this.InitializeComponent();
        }

        private async void App_Drop(object sender, DragEventArgs e) {
            Debug.WriteLine("Dropped success!");
            var file = await StorageFile.GetFileFromPathAsync(
                $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}\\AppData\\Local\\ASUS\\ScreenXpert\\ScreenPadLauncher.ini");

            var lines = await FileIO.ReadLinesAsync(file);
            
            var sections = new Dictionary<string, Dictionary<string, string>>();
            var activeSection = "";
            foreach (var line in lines.Where(x => !string.IsNullOrEmpty(x))) {
                if (line.StartsWith('[') && line.EndsWith(']')) {
                    activeSection = line;
                } else {
                    if (!sections.TryGetValue(activeSection, out var section)) {
                        section = new Dictionary<string, string>();
                        sections[activeSection] = section;
                    }

                    var splitted = line.Split("=", 2);
                    section[splitted[0]] = splitted[1];
                }
            }

            if (!(sender is Button button)) return;

            var number = button.Name.Split("_")[1].ToInt();

            var exe = (await e.DataView.GetStorageItemsAsync())[0] as StorageFile;
            var program = sections[$"[Page0-{number}]"];
            program["ExeAbsolutePath"] = exe.Path;
            program["AppName"] = exe.DisplayName;
            program["ColorA"] = "255";
            program["ColorR"] = "0";
            program["ColorG"] = "0";
            program["ColorB"] = "0";

            await FileIO.WriteLinesAsync(file, GetUpdatedSections());

            var processes = Process.GetProcessesByName("AsusScreenPad.exe");
            foreach (var process in processes) {
                process.Kill();
            }

            IEnumerable<string> GetUpdatedSections() {
                var sects = new List<string>();
                sects.AddRange(GetUpdatedSection("[Info]"));
                sects.AddRange(GetUpdatedSection("[Setting]"));

                for (var i = 0; sections.ContainsKey($"[App{i}]"); i++) {
                    sects.AddRange(GetUpdatedSection($"[App{i}]"));
                }
                
                sects.AddRange(GetUpdatedSection("[Update]"));

                for (var page = 0; sections.ContainsKey($"[Page{page}-0]"); page++) {
                    for (var app = 0; sections.ContainsKey($"[Page{page}-{app}]"); app++) {
                        sects.AddRange(GetUpdatedSection($"[Page{page}-{app}]"));
                    }
                }
                
                return sects;
            }

            IEnumerable<string> GetUpdatedSection(string sectionName) {
                yield return sectionName;
                foreach (var (key, value) in sections[sectionName]) {
                    yield return $"{key}={value}";
                }
            }
        }

        private void App_DragEnter(object sender, DragEventArgs e) {
            e.DragUIOverride.Caption = "Only .exe allowed";
            e.DragUIOverride.IsGlyphVisible = true;
            e.DragUIOverride.IsContentVisible = true;
            e.DragUIOverride.IsCaptionVisible = true;

            e.AcceptedOperation = DataPackageOperation.None;
            
            var items = e.DataView.GetStorageItemsAsync().AsTask().Result;
            Debug.WriteLine($"Count: {items.Count}");

            if (items.Count != 1) return;
            
            var file = items[0] as StorageFile;
            if (file?.FileType != ".exe") return;
            
            e.AcceptedOperation = DataPackageOperation.Copy;
            e.DragUIOverride.Caption = "Drop here to add";
        }
    }
}